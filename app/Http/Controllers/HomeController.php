<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Auth::user()->category;
        if($category == 's'){

            $profile_check = DB::table('info_students')->where('user_id',Auth::id())->exists();
            if($profile_check === FALSE){
                return redirect('student-profile-setup-step-01');
            }
            return redirect('your-profile');
        }else if($category == 't'){

            $profile_check = DB::table('info_teachers')->where('teacher_id',Auth::id())->exists();
            if($profile_check === FALSE){
                return redirect('teacher-profile-setup-step-01');
            }
            return redirect('teacher-profile');
        }

        return view('home');
    }
}
