<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Language;
use App\Info_student;
use App\Info_teacher;
use App\Teacher_course;
use App\Country;
use App\User;
use App\City;
use App\Degree;
use App\Course;

class StudentController extends Controller
{
    public function profile_setup(Request $request){

        if($request->input('submit_language')){

            $info_student = new Info_student;
            $info_student->user_id = Auth::id();
            $info_student->is_language_id = $request->input('language');
            $info_student->save();

            return redirect('student-profile-setup-step-02');
        }

        $languages = Language::all();
        return view('student.profile_setup.step_1')->with('languages',$languages);
    }

    public function profile_setup_2(Request $request){

        if($request->input('submit_region')){
            $update = array(
                'is_country' => $request->input('country'),
                'is_city' => $request->input('city'),
                'time_zone' => $request->input('time_zone')
            );

            Info_student::where('user_id', Auth::id())
                        ->update($update);
            return redirect('student-profile-setup-step-03');
        }
        
        $data = array(
            'countries' => Country::all(),
            'cities' => City::all()
        );
        return view('student.profile_setup.step_2')->with($data);
    }

    public function profile_setup_3(Request $request){

        if($request->input('submit_personal')){
            $imageName = time().'.'.$request->file('picture')->getClientOriginalExtension();
            $request->file('picture')->move(public_path('fotoupload'), $imageName);

            $update = array(
                'is_dob' => $request->input('dob'),
                'is_gender' => $request->input('gender'),
                'is_phone' => $request->input('phone'),
                'is_photo' => $imageName,
                'is_st_intro' => $request->input('info')
            );
            Info_student::where('user_id', Auth::id())
                        ->update($update);
            return redirect('student-profile-setup-step-04');
        }

        return view('student.profile_setup.step_3');
    }

    public function profile_setup_4(Request $request){

        if($request->input('submit_academic')){
            $update = array(
                'is_qualification' => $request->input('degree'),
                'is_institute' => $request->input('institute'),
                'is_from' => $request->input('from'),
                'is_to' => $request->input('to')
            );
            Info_student::where('user_id', Auth::id())
                        ->update($update);
            return redirect('your-profile');
        }

        $data = array(
            'degrees' => Degree::all()
        );
        return view('student.profile_setup.step_4')->with($data);
    }

    public function profile(){
        
        //$info = User::find(Auth::id())->info_student;

        //$info = Info_student::find(Auth::id())->user;

        $info = Info_student::info_join();

        return view('student.profile.profile')->with('user', $info);
    }

    public function edit_profile(Request $request){
        
        if($request->input('submit_language')){
            $update = array(
                'is_language_id' => $request->input('language')
            );

            Info_student::where('user_id', Auth::id())
                        ->update($update);

        }else if($request->input('submit_region')){
            $update = array(
                'is_country' => $request->input('country'),
                'is_city' => $request->input('city'),
                'time_zone' => $request->input('time_zone')
            );

            Info_student::where('user_id', Auth::id())
                        ->update($update);
                 
        }else if($request->input('submit_personal')){

            
            if(empty($request->file('picture'))){
                $update = array(
                    'is_dob' => $request->input('dob'),
                    'is_gender' => $request->input('gender'),
                    'is_phone' => $request->input('phone'),
                    'is_st_intro' => $request->input('info')
                ); 
            }else{
                $imageName = time().'.'.$request->file('picture')->getClientOriginalExtension();
                $request->file('picture')->move(public_path('fotoupload'), $imageName);
                $update = array(
                    'is_dob' => $request->input('dob'),
                    'is_gender' => $request->input('gender'),
                    'is_phone' => $request->input('phone'),
                    'is_photo' => $imageName,
                    'is_st_intro' => $request->input('info')
                );
            }
            Info_student::where('user_id', Auth::id())
                        ->update($update);

        }else if($request->input('submit_academic')){
            $update = array(
                'is_qualification' => $request->input('degree'),
                'is_institute' => $request->input('institute'),
                'is_from' => $request->input('from'),
                'is_to' => $request->input('to')
            );
            Info_student::where('user_id', Auth::id())
                        ->update($update);
        }

        $data = array(
            'user' => Info_student::info_join(),
            'degrees' => Degree::all(),
            'countries' => Country::all(),
            'cities' => City::all(),
            'languages' => Language::all()
        );
        return view('student.profile.edit')->with($data);
    }
    
    public function find_teacher(Request $request){

        if($request->input('course_select')){
            $course_id = $request->input('course_select');
        }else{
            $course_id = FALSE;
        }

        $data = array(
            'courses' => Course::all(),
            'tc' => Teacher_course::teacher_courses($course_id),
        );
        return view('student.find_teacher')->with($data);
    }
}
