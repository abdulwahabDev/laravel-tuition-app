<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Info_teacher;
use App\Language;
use App\Country;
use App\User;
use App\City;
use App\Degree;
use App\Course;
use App\Education;
use App\Work_experience;
use App\Certificate;
use App\Teacher_course;

class TeacherController extends Controller
{
    public function profile_setup(Request $request){

        if($request->input('submit_language')){

            $info_teacher = new Info_teacher;
            $info_teacher->teacher_id = Auth::id();
            $info_teacher->it_language = $request->input('language');
            $info_teacher->save();

            return redirect('teacher-profile-setup-step-02');
        }

        $languages = Language::all();
        return view('teacher.profile_setup.step_1')->with('languages',$languages);
    }

    
    public function profile_setup_2(Request $request){

        if($request->input('submit_region')){
            $update = array(
                'it_country' => $request->input('country'),
                'it_city' => $request->input('city'),
                'time_zone' => $request->input('time_zone')
            );

            Info_teacher::where('teacher_id', Auth::id())
                        ->update($update);
            return redirect('teacher-profile-setup-step-03');
        }
        
        $data = array(
            'countries' => Country::all(),
            'cities' => City::all()
        );
        return view('teacher.profile_setup.step_2')->with($data);
    }

    public function profile_setup_3(Request $request){

        if($request->input('submit_personal')){
            $imageName = time().'.'.$request->file('picture')->getClientOriginalExtension();
            $request->file('picture')->move(public_path('fotoupload'), $imageName);

            $update = array(
                'it_dob' => $request->input('dob'),
                'it_gender' => $request->input('gender'),
                'it_phone' => $request->input('phone'),
                'it_photo' => $imageName,
                'it_intro' => $request->input('info')
            );
            Info_teacher::where('teacher_id', Auth::id())
                        ->update($update);
            return redirect('teacher-profile-setup-step-04');
        }

        return view('teacher.profile_setup.step_3');
    }

    public function profile_setup_4(Request $request){

        if($request->input('submit_academic')){
            $update = array(
                'it_qualification' => $request->input('degree'),
                'it_institute' => $request->input('institute'),
                'it_from' => $request->input('from'),
                'it_to' => $request->input('to')
            );
            Info_teacher::where('teacher_id', Auth::id())
                        ->update($update);
            return redirect('teacher-profile-setup-step-05');
        }

        $data = array(
            'degrees' => Degree::all()
        );
        return view('teacher.profile_setup.step_4')->with($data);
    }

    public function profile_setup_5(Request $request){

        if($request->input('submit_teach_info')){
           
            $this->validate($request, [
                'courses' => 'required',
                'link' => 'required',
                'webcam' => 'required',
                'teach_spec' => 'required',
            ]);
            
            $courses = implode(",", $request->input('courses'));

            foreach($request->input('courses') as $course){
                $teacher_course = new Teacher_course;

                $teacher_course->tc_course_id = $course;
                $teacher_course->tc_teacher_id = Auth::id();
                $teacher_course->save();
            }

            $teach_spec= implode(",", $request->input('teach_spec'));

            $update = array(
                'it_video_link' => $request->input('link'),
                'it_webcam' => $request->input('webcam'),
                'it_teaching_specialties' => $teach_spec,
                'it_courses' => $courses,
            );

            Info_teacher::where('teacher_id', Auth::id())
                        ->update($update);

            return redirect('teacher-profile-setup-step-06');
        }

        $data = array(
            'courses' => Course::all()
        );
        return view('teacher.profile_setup.step_5')->with($data);
    }

    public function profile_setup_6(Request $request){

        if($request->input('submit_education')){
            $this->validate($request, [
                'degree' => 'required',
                'major' => 'required',
                'institute' => 'required',
                'country' => 'required',
                'city' => 'required',
                'date' => 'required',
                'attachment' => 'required',
                'description' => 'required',
            ]);
            
            $doc = time().'.'.$request->file('attachment')->getClientOriginalExtension();
            $request->file('attachment')->move(public_path('documents'), $doc);

            $education = new Education;
            $education->e_teacher_id = Auth::id();
            $education->e_degree_id = $request->input('degree');
            $education->e_major = $request->input('major');
            $education->e_institution = $request->input('institute');
            $education->e_country_id = $request->input('country');
            $education->e_city_id = $request->input('city');
            $education->e_completion_year = $request->input('date');
            $education->e_description = $request->input('description');
            $education->e_doc = $doc;
            $education->save();

            return redirect('teacher-profile-setup-step-07');
            
        }

        $data = array(
            'degrees' => Degree::all(),
            'countries' => Country::all(),
            'cities' => City::all(),
        );

        return view('teacher.profile_setup.step_6')->with($data);
    }

    public function profile_setup_7(Request $request){

        if($request->input('submit_work')){
            $this->validate($request, [
                'position' => 'required',
                'company' => 'required',
                'from' => 'required',
                'to' => 'required',
                'country' => 'required',
                'city' => 'required',
                'current' => 'required',
                'description' => 'required',
            ]);
            
            $work_experience = new Work_experience;
            $work_experience->we_teacher_id = Auth::id();
            $work_experience->we_position = $request->input('position');
            $work_experience->we_company = $request->input('company');
            $work_experience->we_from = $request->input('from');
            $work_experience->we_to = $request->input('to');
            $work_experience->we_country_id = $request->input('country');
            $work_experience->we_city_id = $request->input('city');
            $work_experience->we_current = $request->input('current');
            $work_experience->we_description = $request->input('description');
            $work_experience->save();
            
            return redirect('teacher-profile-setup-step-08');
        }

        $data = array(
            'degrees' => Degree::all(),
            'countries' => Country::all(),
            'cities' => City::all(),
        );
        return view('teacher.profile_setup.step_7')->with($data);
    }

    public function profile_setup_8(Request $request){
        if($request->input('submit_certificate')){
            $this->validate($request, [
                'certificate' => 'required',
                'institute' => 'required',
                'receive' => 'required',
                'description' => 'required',
                'document' => 'required',
            ]);
            
            $doc = time().'.'.$request->file('document')->getClientOriginalExtension();
            $request->file('document')->move(public_path('documents'), $doc);

            $certificate = new Certificate;
            $certificate->cer_teacher_id = Auth::id();
            $certificate->cer_name = $request->input('certificate');
            $certificate->cer_institute = $request->input('institute');
            $certificate->cer_receiving = $request->input('receive');
            $certificate->cer_description = $request->input('description');
            $certificate->cer_doc = $doc;
            $certificate->save();

            return redirect('teacher-profile');
        }
        $data = array(
            'degrees' => Degree::all(),
            'countries' => Country::all(),
            'cities' => City::all(),
        );
        return view('teacher.profile_setup.step_8')->with($data);
    }

    public function profile(){

        $info = Info_teacher::info_join();

        return view('teacher.profile.profile')->with('user', $info);
    }

    public function edit_profile(Request $request){
        
        if($request->input('submit_language')){
            $update = array(
                'it_language' => $request->input('language')
            );

            Info_teacher::where('teacher_id', Auth::id())
                        ->update($update);

        }else if($request->input('submit_region')){
            $update = array(
                'it_country' => $request->input('country'),
                'it_city' => $request->input('city'),
                'time_zone' => $request->input('time_zone')
            );

            Info_teacher::where('teacher_id', Auth::id())
                        ->update($update);
                 
        }else if($request->input('submit_personal')){

            
            if(empty($request->file('picture'))){
                $update = array(
                    'it_dob' => $request->input('dob'),
                    'it_gender' => $request->input('gender'),
                    'it_phone' => $request->input('phone'),
                    'it_intro' => $request->input('info')
                ); 
            }else{
                $imageName = time().'.'.$request->file('picture')->getClientOriginalExtension();
                $request->file('picture')->move(public_path('fotoupload'), $imageName);
                $update = array(
                    'it_dob' => $request->input('dob'),
                    'it_gender' => $request->input('gender'),
                    'it_phone' => $request->input('phone'),
                    'it_photo' => $imageName,
                    'it_intro' => $request->input('info')
                );
            }
            Info_teacher::where('teacher_id', Auth::id())
                        ->update($update);

        }else if($request->input('submit_academic')){
            $update = array(
                'it_qualification' => $request->input('degree'),
                'it_institute' => $request->input('institute'),
                'it_from' => $request->input('from'),
                'it_to' => $request->input('to')
            );
            Info_teacher::where('teacher_id', Auth::id())
                        ->update($update);
        }

        $data = array(
            'user' => Info_teacher::info_join(),
            'degrees' => Degree::all(),
            'countries' => Country::all(),
            'cities' => City::all(),
            'languages' => Language::all()
        );
        return view('teacher.profile.edit')->with($data);
    }
}
