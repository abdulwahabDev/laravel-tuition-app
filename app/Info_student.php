<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Info_student extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public static function info_join(){
        $user = DB::table('info_students')
            ->join('users', 'info_students.user_id', '=', 'users.id')
            ->join('cities', 'info_students.is_city', '=', 'cities.city_id')
            ->join('countries', 'info_students.is_country', '=', 'countries.count_id')
            ->join('degrees', 'info_students.is_qualification', '=', 'degrees.deg_id')
            ->join('languages', 'info_students.is_language_id', '=', 'languages.lang_id')
            ->select('*')
            ->where('info_students.user_id',Auth::id())
            ->get()->first();
            return $user;
    } 
}
