<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Info_teacher extends Model
{
    public static function info_join(){
        $user = DB::table('info_teachers')
            ->join('users', 'info_teachers.teacher_id', '=', 'users.id')
            ->join('cities', 'info_teachers.it_city', '=', 'cities.city_id')
            ->join('countries', 'info_teachers.it_country', '=', 'countries.count_id')
            ->join('degrees', 'info_teachers.it_qualification', '=', 'degrees.deg_id')
            ->join('languages', 'info_teachers.it_language', '=', 'languages.lang_id')
            ->select('*')
            ->where('info_teachers.teacher_id',Auth::id())
            ->get()->first();

        return $user;
    }

    public static function teacher_courses(){
        $courses = DB::table('teacher_courses')
            ->join('info_teachers', 'teacher_courses.tc_teacher_id', '=', 'info_teachers.teacher_id')
            ->select('*')
            ->where('teacher_courses.tc_teacher_id',Auth::id())
            ->get();

         return $courses;
    }
}
