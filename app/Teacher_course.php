<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Teacher_course extends Model
{
    
    public static function teacher_courses($course_id = FALSE){
        
        if($course_id === FALSE){
            $courses = DB::table('teacher_courses')
            ->join('info_teachers', 'teacher_courses.tc_teacher_id', '=', 'info_teachers.teacher_id')
            ->join('courses', 'teacher_courses.tc_course_id', '=', 'courses.course_id')
            ->join('users', 'teacher_courses.tc_teacher_id', '=', 'users.id')
            ->select('*')
            ->get();

         return $courses;
        }

        $courses = DB::table('teacher_courses')
            ->join('info_teachers', 'teacher_courses.tc_teacher_id', '=', 'info_teachers.teacher_id')
            ->join('courses', 'teacher_courses.tc_course_id', '=', 'courses.course_id')
            ->join('users', 'teacher_courses.tc_teacher_id', '=', 'users.id')
            ->select('*')
            ->where('teacher_courses.tc_course_id',$course_id)
            ->get();

         return $courses;
    }
}
