<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_students', function (Blueprint $table) {
            $table->increments('is_id');
			
			$table->mediuminteger('user_id')->nullable();
            $table->date('is_dob')->nullable();
            $table->string('is_phone',20)->nullable();
            $table->string('is_gender',8)->nullable();
            $table->string('is_city',20)->nullable();
            $table->string('is_country',20)->nullable();
			
			$table->string('is_qualification',20)->nullable();
			$table->string('is_institute',20)->nullable();
			$table->string('is_from',20)->nullable();
			$table->string('is_to',20)->nullable();
			$table->text('is_st_intro')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_students');
    }
}
