<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_teachers', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('teacher_id')->nullable();
            $table->date('it_dob')->nullable();
            $table->string('it_phone',20)->nullable();
            $table->string('it_gender',8)->nullable();
            $table->integer('it_city')->nullable();
            $table->integer('it_country')->nullable();
			$table->integer('it_qualification')->nullable();
			$table->string('it_institute',20)->nullable();
			$table->integer('it_from')->nullable();
			$table->integer('it_to')->nullable();
			$table->text('it_intro')->nullable();
			$table->integer('it_language')->nullable();
			$table->float('time_zone',10,2)->nullable();
			$table->string('it_photo',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_teachers');
    }
}
