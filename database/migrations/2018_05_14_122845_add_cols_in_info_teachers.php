<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsInInfoTeachers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('info_teachers', function (Blueprint $table) {
            $table->string('it_degree_doc',50)->nullable();
            $table->string('it_teach_cert_doc',50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('info_teachers', function (Blueprint $table) {
            //
        });
    }
}
