<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreTeacherInfoCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('info_teachers', function (Blueprint $table) {
            //
            $table->string('it_video_link',255);
            $table->boolean('it_webcam');
            $table->text('it_teaching_specialties');
            $table->text('it_courses');
            $table->boolean('it_verified');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('info_teachers', function (Blueprint $table) {
            //
        });
    }
}
