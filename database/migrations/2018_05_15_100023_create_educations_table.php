<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educations', function (Blueprint $table) {
            $table->increments('e_id');
            $table->integer('e_degree_id');
            $table->string('e_major',50);
            $table->string('e_institution',50);
            $table->integer('e_city_id');
            $table->integer('e_country_id');
            $table->string('e_completion_year',10);
            $table->text('e_description');
            $table->string('e_doc',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('educations');
    }
}
