<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_experiences', function (Blueprint $table) {
            $table->increments('we_id');
            $table->string('we_position',50);
            $table->string('we_company',50);
            $table->date('we_from');
            $table->date('we_to');
            $table->boolean('we_current');
            $table->integer('we_city_id');
            $table->integer('we_country_id');
            $table->text('we_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_experiences');
    }
}
