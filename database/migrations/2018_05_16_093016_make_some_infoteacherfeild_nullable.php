<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeSomeInfoteacherfeildNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('info_teachers', function (Blueprint $table) {
            $table->string('it_video_link',255)->nullable()->change();
            $table->boolean('it_webcam')->nullable()->change();
            $table->text('it_teaching_specialties')->nullable()->change();
            $table->text('it_courses')->nullable()->change();
            $table->boolean('it_verified')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('info_teachers', function (Blueprint $table) {
            //
        });
    }
}
