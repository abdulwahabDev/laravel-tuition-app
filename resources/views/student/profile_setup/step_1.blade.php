@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Setup  Step 01</div>

                <div class="panel-body">
                        <div class="row justify-content-center">
                            <h2>Language</h2>
                            <br>
                            <br>
                            <div class='container'>
                            {!! Form::open([ 'action' => "StudentController@profile_setup" , 'method'=> 'POST', 'class' => 'form-horizontal']) !!}
                                <input type="hidden" name="student_id" value="{{ Auth::id() }}"> 
                                <div class="row">
                                    <div class="col-sm-offset-2 col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Select Your Language:</label>
                                            <select class="form-control" name="language" >
                                                <option value="">-select-</option>
                                                @foreach($languages as $language)
                                                    <option value="{{ $language->lang_id }}">{{ $language->language }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-sm-offset-2">
                                    <div class="form-group">        
                                        <input type="submit" class="btn btn-info  btn-md" name="submit_language" />
                                    </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
