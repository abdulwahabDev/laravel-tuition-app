@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Setup  Step 03</div>

                <div class="panel-body">
                        <div class="row justify-content-center">
                            <h2>Personal Information</h2>
                            <br>
                            <br>
                            <div class='container'>
                            {!! Form::open([ 'action' => "StudentController@profile_setup_3" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                                
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">DOB:</label>
                                            <input type='date' name='dob' class='form-control' />
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label">Gender:</label>
                                                <select class="form-control" name="gender" >
                                                        <option value="">-select-</option>
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                          <label class="control-label">Phone:</label>
                                          <input type='text' name='phone' class='form-control' placeholder="Enter phone no.."/>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">Picure:</label>
                                            {{Form::file('picture')}}
                                        </div>
                                    </div>
                                </div>

                                <div class='row'>
                                    <div class='col-sm-offset-1 col-sm-5'>
                                        <div class="form-group">
                                        <label class="control-label">Write short introduction abour youself:</label>
                                        {{Form::textarea('info', '',['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Type youn introduction'])}}
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-4">
                                    <div class="form-group">        
                                        <input type="submit" class="btn btn-info  btn-md" name="submit_personal" />
                                    </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
