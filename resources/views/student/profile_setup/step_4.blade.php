@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Setup  Step 04</div>

                <div class="panel-body">
                        <div class="row justify-content-center">
                            <h2>Academic Information</h2>
                            <br>
                            <br>
                            <div class='container'>
                            {!! Form::open([ 'action' => "StudentController@profile_setup_4" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                                
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">Degree:</label>
                                            <select class="form-control" name="degree" >
                                                <option value="">-select-</option>
                                                @foreach($degrees as $degree)
                                                    <option value="{{ $degree->deg_id }}">{{ $degree->degree }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">Institute:</label>
                                            <input type='text' class='form-control' name='institute' placeholder="Type institute name.." />
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">From year:</label>
                                            <select class="form-control" name="from" >
                                                <option value="">-select-</option>
                                                 @for ($i = 2018; $i >= 1970; $i--)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                 @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">To year:</label>
                                            <select class="form-control" name="to" >
                                                <option value="">-select-</option>
                                                <option value="progress">In Progress</option>
                                                @for ($i = 2018; $i >= 1970; $i--)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-4">
                                    <div class="form-group">        
                                        <input type="submit" class="btn btn-info  btn-md" name="submit_academic" />
                                    </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
