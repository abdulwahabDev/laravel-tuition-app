@extends('layouts.app')

@section('content')
<hr class="">
<div class="container target">
    <div class="row">
        <div class="col-sm-10">
             <h1 class="">{{$user->name}}</h1>
        
        <button type="button" class="btn btn-success"></button>  <a href="{{url('edit-teacher-profile')}}" class="btn btn-info">Edit profile</a>
    <br>
        </div>
      <div class="col-sm-2"><a href="<?=url('fotoupload'.'/'.$user->it_photo)?>" class="pull-right"><img title="profile image" class="img-circle img-responsive" src="<?=url('fotoupload'.'/'.$user->it_photo)?>"></a>

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-3">
            <!--left col-->
            <ul class="list-group">
                <li class="list-group-item text-muted" contenteditable="false">Profile</li>
                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Language</strong></span> {{$user->language}}</li>
                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Gender</strong></span> {{$user->it_gender}}</li>
            <li class="list-group-item text-right"><span class="pull-left"><strong class="">Lives in</strong></span> {{$user->city}}, {{$user->country}}</li>
              <li class="list-group-item text-right"><span class="pull-left"><strong class="">Qualification</strong></span> {{$user->degree}}</li>
            </ul>
           <div class="panel panel-default">
             <div class="panel-heading">Insured / Bonded?

                </div>
                <div class="panel-body"><i style="color:green" class="fa fa-check-square"></i> Yes, I am insured and bonded.

                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Website <i class="fa fa-link fa-1x"></i>

                </div>
                <div class="panel-body"><a href="http://bootply.com" class="">bootply.com</a>

                </div>
            </div>
          
            <ul class="list-group">
                <li class="list-group-item text-muted">Activity <i class="fa fa-dashboard fa-1x"></i>

                </li>
                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Shares</strong></span> 125</li>
                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Likes</strong></span> 13</li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong class="">Posts</strong></span> 37</li>
                        <li class="list-group-item text-right"><span class="pull-left"><strong class="">Followers</strong></span> 78</li>
            </ul>
            <div class="panel panel-default">
                <div class="panel-heading">Social Media</div>
                <div class="panel-body">	<i class="fa fa-facebook fa-2x"></i>  <i class="fa fa-github fa-2x"></i> 
                    <i class="fa fa-twitter fa-2x"></i> <i class="fa fa-pinterest fa-2x"></i>  <i class="fa fa-google-plus fa-2x"></i>

                </div>
            </div>
        </div>
        <!--/col-3-->
        <div class="col-sm-9" contenteditable="false" style="">
            <div class="panel panel-default">
           
                <div class="panel-heading">{{$user->name}}</div>
       
                <div class="panel-body"> {!!$user->it_intro!!}

                </div>
            </div>
            <div class="panel panel-default target">
                    <div class="panel-heading" contenteditable="false">My Lessons</div>
                    <div class="panel-body">   
                </div>
                     
            </div>
                  
        </div>
            <div class="panel panel-default">
                <div class="panel-heading">Courses</div>
                <div class="panel-body"> A long description about me.
    
                </div>
            </div>
        </div>
    </div>
    @endsection