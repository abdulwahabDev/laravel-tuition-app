@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Setup  Step 01</div>

                <div class="panel-body">
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:12.5%">
                                12.5% Profile Complete
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <h2>Language</h2>
                            <br>
                            <br>
                            <div class='container'>
                            {!! Form::open([ 'action' => "TeacherController@profile_setup" , 'method'=> 'POST', 'class' => 'form-horizontal']) !!}
                                <input type="hidden" name="student_id" value="{{ Auth::id() }}"> 
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-5">
                                        <div class="form-group">
                                          <label class="control-label">Select Your Language:</label>
                                            <select class="form-control" name="language" >
                                                <option value="">-select-</option>
                                                @foreach($languages as $language)
                                                    <option value="{{ $language->lang_id }}">{{ $language->language }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-4">
                                        <div class="form-group">        
                                        <a class="btn btn-info  btn-md" href="{{url('teacher-profile-setup-step-02')}}">Skip</a>
                                        </div>
                                    </div>

                                    <div class="col-sm-offset-1 col-sm-4">
                                        <div class="form-group">        
                                            <input value="Next" type="submit" class="btn btn-info  btn-md" name="submit_language" />
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
