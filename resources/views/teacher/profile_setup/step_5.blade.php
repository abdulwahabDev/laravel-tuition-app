@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Setup  Step 05</div>
                <div class="panel-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:62.5%">
                            62.5% Profile Complete
                        </div>
                    </div>
                        <div class="row justify-content-center">
                            <h2>Teaching Information</h2>
                            <br>
                            <br>
                            <div class='container'>
                            {!! Form::open([ 'action' => "TeacherController@profile_setup_5" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                                <h4>Select courses your gonna teach</h4>
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                           <!-- <label class="control-label">Courses:</label> -->
                                            @foreach($courses as $course)
                                            <label class="control-label">{{$course->course}}:</label>
                                            {{Form::checkbox('courses[]', $course->course_id)}}
                                            <br>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <h4>Video introduction</h4>
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-3">
                                        <div class="form-group">
                                          <label class="control-label">Copy your introduction video link here:</label>
                                          <input type='text' name='link' class='form-control' placeholder="Paste link here.."/>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">Do you have webcam:</label><br>
                                            {{Form::radio('webcam', 1)}} Yes
                                            {{Form::radio('webcam', 0)}} No
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <h4>Teaching Specialties</h4>
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-3">
                                        <div class="form-group">
                                           <!-- <label class="control-label">Courses:</label> -->
                                           <div id="checkboxes">
                                                <input type="checkbox" name='teach_spec[]' value='Beginners: I have experience teaching absolute beginners' /> Beginners: I have experience teaching absolute beginnerss<br />
                                                <input type="checkbox"  name='teach_spec[]' value='Advanced Teaching: I have experience advance level teaching' /> Advanced Teaching: I have experience advance level teaching<br />
                                                <input type="checkbox"  name='teach_spec[]' value='Test Preparation: I have experience teaching the language for test preparation' /> Test Preparation: I have experience teaching the language for test preparation<br />
                                            </div>                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">Add Other:</label>
                                            <input class='form-control' type="text" id="newCheckText" /> 
                                            <br>
                                            <a class="btn btn-warning  btn-sm" id="addCheckbox">Add your own specialty</a>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-4">
                                        <div class="form-group">        
                                        <!-- <a class="btn btn-info  btn-md" href="{{url('teacher-profile-setup-step-04')}}">Skip</a> -->
                                        </div>
                                    </div>
            
                                    <div class="col-sm-offset-1 col-sm-4">
                                    <div class="form-group">        
                                        <input type="submit" class="btn btn-info  btn-md" name="submit_teach_info" />
                                    </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
