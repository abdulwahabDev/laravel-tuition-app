@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Setup  Step 08</div>

                <div class="panel-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                            100% Profile Complete after this step
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <h2>Certificate</h2>
                        <br>
                        <br>
                        {!! Form::open([ 'action' => "TeacherController@profile_setup_8" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                            
                            <div class='row'>
                                <div class="col-sm-offset-1 col-sm-10">
                                    <div class="form-group">
                                        <label class="control-label">Certificate name:</label>
                                        <input type='text' name='certificate' class='form-control' placeholder="Enter certificate name.."/>
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class="col-sm-offset-1 col-sm-10">
                                    <div class="form-group">
                                        <label class="control-label">Institute:</label>
                                        <input type='text' name='institute' class='form-control' placeholder="Enter institute no.."/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-offset-1 col-sm-10">
                                    <div class="form-group">
                                        <label class="control-label">Certificate Receiving date:</label>
                                        <input type='date' name='receive' class='form-control' />
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class='col-sm-offset-1 col-sm-10'>
                                    <div class="form-group">
                                    <label class="control-label">Description:</label>
                                    {{Form::textarea('description', '',['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Type description'])}}
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class='row'>
                                <div class="col-sm-offset-1 col-sm-10">
                                    <div class="form-group">
                                        <label class="control-label">Attachment:</label>  
                                        {{Form::file('document')}}
                                    </div>
                                </div>
                            </div>
                            
                            <div class='row'>
                                <div class="col-sm-offset-8 col-sm-1">
                                    <div class="form-group">        
                                        <input value="Press to complepte your profile" type="submit" class="btn btn-warning  btn-md" name="submit_certificate" />
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        
                                    
                    </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
