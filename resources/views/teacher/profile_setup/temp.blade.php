@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Setup  Step 06</div>

                <div class="panel-body">
                        <div class="row justify-content-center">
                            <h2>Professional Teaching Background</h2>
                            <br>
                            <br>
                            <div class="container">
                                <h3>Add Education</h3>
                                <!-- Trigger the modal with a button -->
                                <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal">Education Add+</button>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Education</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!! Form::open([ 'action' => "TeacherController@profile_setup_6" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                            
                                            <div class='row'>
                                                <div class="col-sm-offset-1 col-sm-10">
                                                    <div class="form-group">
                                                        <label class="control-label">Degree level:</label>
                                                        <select class="form-control" name="degree" >
                                                            <option value="">-select-</option>
                                                            @foreach($degrees as $degree)
                                                                <option value="{{ $degree->deg_id }}">{{ $degree->degree }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class='row'>
                                                <div class="col-sm-offset-1 col-sm-10">
                                                    <div class="form-group">
                                                        <label class="control-label">Major Subject:</label>
                                                        <input type='text' name='major' class='form-control' placeholder="Enter major no.."/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class='row'>
                                                <div class="col-sm-offset-1 col-sm-10">
                                                    <div class="form-group">
                                                        <label class="control-label">Institute:</label>
                                                        <input type='text' name='institute' class='form-control' placeholder="Enter institute no.."/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-offset-1 col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Country:</label>
                                                        <select class="form-control" name="country" >
                                                            <option value="">-select-</option>
                                                            @foreach($countries as $country)
                                                                <option value="{{ $country->count_id }}">{{ $country->country }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-offset-1 col-sm-5">
                                                    <div class="form-group">
                                                        <label class="control-label">City:</label>
                                                        <select class="form-control" name="city" >
                                                            <option value="">-select-</option>
                                                            @foreach($cities as $city)
                                                                <option value="{{ $city->city_id }}">{{ $city->city }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class='row'>
                                                <div class="col-sm-offset-1 col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Completion year:</label>
                                                        <input type='date' name='date' class='form-control' />
                                                    </div>
                                                </div>

                                                <div class="col-sm-offset-1 col-sm-5">
                                                    <div class="form-group">
                                                        <label class="control-label">Attachment:</label>
                                                        {{Form::file('attachment')}}
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class='row'>
                                                <div class='col-sm-offset-1 col-sm-10'>
                                                    <div class="form-group">
                                                    <label class="control-label">Description:</label>
                                                    {{Form::textarea('description', '',['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Type description'])}}
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class='row'>
                                                <div class="col-sm-offset-10 col-sm-1">
                                                    <div class="form-group">        
                                                        <input value="Save" type="submit" class="btn btn-info  btn-md" name="submit_education" />
                                                    </div>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>

                            <div class="container">
                                <h3>Add Work Experience</h3>
                                <!-- Trigger the modal with a button -->
                                <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModalTwo">Work Experience Add+</button>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="myModalTwo" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Work Experience</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!! Form::open([ 'action' => "TeacherController@profile_setup_6" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                            
                                            <div class='row'>
                                                <div class="col-sm-offset-1 col-sm-10">
                                                    <div class="form-group">
                                                        <label class="control-label">Job title/Position:</label>
                                                        <input type='text' name='position' class='form-control' placeholder="Enter job title no.."/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class='row'>
                                                <div class="col-sm-offset-1 col-sm-10">
                                                    <div class="form-group">
                                                        <label class="control-label">Company:</label>
                                                        <input type='text' name='company' class='form-control' placeholder="Enter company name no.."/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-offset-1 col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">From:</label>
                                                        <input type='date' name='from' class='form-control' />
                                                    </div>
                                                </div>
                                                <div class="col-sm-offset-1 col-sm-5">
                                                    <div class="form-group">
                                                        <label class="control-label">to:</label>
                                                        <input type='date' name='to' class='form-control' />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-offset-1 col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Country:</label>
                                                        <select class="form-control" name="country" >
                                                            <option value="">-select-</option>
                                                            @foreach($countries as $country)
                                                                <option value="{{ $country->count_id }}">{{ $country->country }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-offset-1 col-sm-5">
                                                    <div class="form-group">
                                                        <label class="control-label">City:</label>
                                                        <select class="form-control" name="city" >
                                                            <option value="">-select-</option>
                                                            @foreach($cities as $city)
                                                                <option value="{{ $city->city_id }}">{{ $city->city }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class='row'>
                                                <div class="col-sm-offset-1 col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Currently Working here:</label>
                                                        <input type='checkbox' name='current' value='1'/>
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class='row'>
                                                <div class='col-sm-offset-1 col-sm-10'>
                                                    <div class="form-group">
                                                    <label class="control-label">Description:</label>
                                                    {{Form::textarea('description', '',['id' => 'article-ckeditor-two', 'class' => 'form-control', 'placeholder' => 'Type description'])}}
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class='row'>
                                                <div class="col-sm-offset-10 col-sm-1">
                                                    <div class="form-group">        
                                                        <input value="Save" type="submit" class="btn btn-info  btn-md" name="submit_work" />
                                                    </div>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>

                            <div class="container">
                                <h3>Add Certification</h3>
                                <!-- Trigger the modal with a button -->
                                <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModalThree">Certificate Add+</button>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="myModalThree" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Certificate</h4>
                                        </div>
                                        <div class="modal-body">
                                                {!! Form::open([ 'action' => "TeacherController@profile_setup_6" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                                
                                                <div class='row'>
                                                    <div class="col-sm-offset-1 col-sm-10">
                                                        <div class="form-group">
                                                            <label class="control-label">Certificate name:</label>
                                                            <input type='text' name='certificate' class='form-control' placeholder="Enter certificate name.."/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class='row'>
                                                    <div class="col-sm-offset-1 col-sm-10">
                                                        <div class="form-group">
                                                            <label class="control-label">Institute:</label>
                                                            <input type='text' name='institute' class='form-control' placeholder="Enter institute no.."/>
                                                        </div>
                                                    </div>
                                                </div>
    
                                                <div class="row">
                                                    <div class="col-sm-offset-1 col-sm-10">
                                                        <div class="form-group">
                                                            <label class="control-label">Certificate Receiving date:</label>
                                                            <input type='date' name='receive' class='form-control' />
                                                        </div>
                                                    </div>
                                                </div>
    
                                                <div class='row'>
                                                    <div class='col-sm-offset-1 col-sm-10'>
                                                        <div class="form-group">
                                                        <label class="control-label">Description:</label>
                                                        {{Form::textarea('description', '',['id' => 'article-ckeditor-three', 'class' => 'form-control', 'placeholder' => 'Type description'])}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>

                                                <div class='row'>
                                                    <div class="col-sm-offset-1 col-sm-10">
                                                        <div class="form-group">
                                                            <label class="control-label">Attachment:</label>  
                                                            {{Form::file('document')}}
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class='row'>
                                                    <div class="col-sm-offset-10 col-sm-1">
                                                        <div class="form-group">        
                                                            <input value="Save" type="submit" class="btn btn-info  btn-md" name="submit_certificate" />
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                {!! Form::open([ 'action' => "TeacherController@profile_setup_6" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                                <div class='row'>
                                    <div class="col-sm-offset-5 col-sm-1">
                                        <div class="form-group">        
                                            <input value="Press to complepte your profile" type="submit" class="btn btn-warning  btn-md" name="profile_completed" />
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
