<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'HomeController@index');
/* STUDENT ROUTES */
Route::match(['get', 'post'], '/student-profile-setup-step-01', 'StudentController@profile_setup');
Route::match(['get', 'post'], '/student-profile-setup-step-02', 'StudentController@profile_setup_2');
Route::match(['get', 'post'], '/student-profile-setup-step-03', 'StudentController@profile_setup_3');
Route::match(['get', 'post'], '/student-profile-setup-step-04', 'StudentController@profile_setup_4');
Route::match(['get', 'post'], '/find-teacher', 'StudentController@find_teacher');
Route::match(['get', 'post'], '/your-profile', 'StudentController@profile');
Route::match(['get', 'post'], '/edit-profile', 'StudentController@edit_profile');

/* TEACHER ROUTES */
Route::match(['get', 'post'], '/teacher-profile-setup-step-01', 'TeacherController@profile_setup');
Route::match(['get', 'post'], '/teacher-profile-setup-step-02', 'TeacherController@profile_setup_2');
Route::match(['get', 'post'], '/teacher-profile-setup-step-03', 'TeacherController@profile_setup_3');
Route::match(['get', 'post'], '/teacher-profile-setup-step-04', 'TeacherController@profile_setup_4');
Route::match(['get', 'post'], '/teacher-profile-setup-step-05', 'TeacherController@profile_setup_5');
Route::match(['get', 'post'], '/teacher-profile-setup-step-06', 'TeacherController@profile_setup_6');
Route::match(['get', 'post'], '/teacher-profile-setup-step-07', 'TeacherController@profile_setup_7');
Route::match(['get', 'post'], '/teacher-profile-setup-step-08', 'TeacherController@profile_setup_8');
Route::match(['get', 'post'], '/teacher-profile', 'TeacherController@profile');
Route::match(['get', 'post'], '/edit-teacher-profile', 'TeacherController@edit_profile');