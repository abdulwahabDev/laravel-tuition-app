<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Setup  Step 04</div>

                <div class="panel-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                            50% Profile Complete
                        </div>
                    </div>
                        
                        <div class="row justify-content-center">
                            <h2>Academic Information</h2>
                            <br>
                            <br>
                            <div class='container'>
                            <?php echo Form::open([ 'action' => "TeacherController@profile_setup_4" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]); ?>

                                
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">Degree:</label>
                                            <select class="form-control" name="degree" >
                                                <option value="">-select-</option>
                                                <?php $__currentLoopData = $degrees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $degree): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($degree->deg_id); ?>"><?php echo e($degree->degree); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">Institute:</label>
                                            <input type='text' class='form-control' name='institute' placeholder="Type institute name.." />
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">From year:</label>
                                            <select class="form-control" name="from" >
                                                <option value="">-select-</option>
                                                 <?php for($i = 2018; $i >= 1970; $i--): ?>
                                                    <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                                 <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">To year:</label>
                                            <select class="form-control" name="to" >
                                                <option value="">-select-</option>
                                                <option value="progress">In Progress</option>
                                                <?php for($i = 2018; $i >= 1970; $i--): ?>
                                                    <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-4">
                                    <div class="form-group">        
                                        <input type="submit" class="btn btn-info  btn-md" name="submit_academic" />
                                    </div>
                                    </div>
                                </div>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>