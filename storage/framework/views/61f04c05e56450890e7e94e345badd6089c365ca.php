<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Setup  Step 03</div>

                <div class="panel-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:37.5%">
                            37.5% Profile Complete
                        </div>
                    </div>
                        <div class="row justify-content-center">
                            <h2>Personal Information</h2>
                            <br>
                            <br>
                            <div class='container'>
                            <?php echo Form::open([ 'action' => "TeacherController@profile_setup_3" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]); ?>

                                
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">DOB:</label>
                                            <input type='date' name='dob' class='form-control' />
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label">Gender:</label>
                                                <select class="form-control" name="gender" >
                                                        <option value="">-select-</option>
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                          <label class="control-label">Phone:</label>
                                          <input type='text' name='phone' class='form-control' placeholder="Enter phone no.."/>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">Picure:</label>
                                            <?php echo e(Form::file('picture')); ?>

                                        </div>
                                    </div>
                                </div>

                                <div class='row'>
                                    <div class='col-sm-offset-1 col-sm-5'>
                                        <div class="form-group">
                                        <label class="control-label">Write short introduction abour youself:</label>
                                        <?php echo e(Form::textarea('info', '',['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Type youn introduction'])); ?>

                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-4">
                                        <div class="form-group">        
                                        <a class="btn btn-info  btn-md" href="<?php echo e(url('teacher-profile-setup-step-04')); ?>">Skip</a>
                                        </div>
                                    </div>
            
                                    <div class="col-sm-offset-1 col-sm-4">
                                    <div class="form-group">        
                                        <input type="submit" class="btn btn-info  btn-md" name="submit_personal" />
                                    </div>
                                    </div>
                                </div>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>