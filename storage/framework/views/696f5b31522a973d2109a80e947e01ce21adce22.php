<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Setup  Step 06</div>

                <div class="panel-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:75%">
                            75% Profile Complete
                        </div>
                    </div>
                        <div class="row justify-content-center">
                            <h2>Education</h2>
                            <br>
                            <br>
            
                            <?php echo Form::open([ 'action' => "TeacherController@profile_setup_6" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]); ?>

            
                            <div class='row'>
                                <div class="col-sm-offset-1 col-sm-10">
                                    <div class="form-group">
                                        <label class="control-label">Degree level:</label>
                                        <select class="form-control" name="degree" >
                                            <option value="">-select-</option>
                                            <?php $__currentLoopData = $degrees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $degree): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($degree->deg_id); ?>"><?php echo e($degree->degree); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class="col-sm-offset-1 col-sm-10">
                                    <div class="form-group">
                                        <label class="control-label">Major Subject:</label>
                                        <input type='text' name='major' class='form-control' placeholder="Enter major no.."/>
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class="col-sm-offset-1 col-sm-10">
                                    <div class="form-group">
                                        <label class="control-label">Institute:</label>
                                        <input type='text' name='institute' class='form-control' placeholder="Enter institute no.."/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-offset-1 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Country:</label>
                                        <select class="form-control" name="country" >
                                            <option value="">-select-</option>
                                            <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($country->count_id); ?>"><?php echo e($country->country); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-offset-1 col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">City:</label>
                                        <select class="form-control" name="city" >
                                            <option value="">-select-</option>
                                            <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($city->city_id); ?>"><?php echo e($city->city); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class="col-sm-offset-1 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Completion year:</label>
                                        <input type='date' name='date' class='form-control' />
                                    </div>
                                </div>

                                <div class="col-sm-offset-1 col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Attachment:</label>
                                        <?php echo e(Form::file('attachment')); ?>

                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class='col-sm-offset-1 col-sm-10'>
                                    <div class="form-group">
                                    <label class="control-label">Description:</label>
                                    <?php echo e(Form::textarea('description', '',['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Type description'])); ?>

                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class='row'>
                                <div class="col-sm-offset-10 col-sm-1">
                                    <div class="form-group">        
                                        <input value="Save" type="submit" class="btn btn-info  btn-md" name="submit_education" />
                                    </div>
                                </div>
                            </div>
                            <?php echo Form::close(); ?>

                                        
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>