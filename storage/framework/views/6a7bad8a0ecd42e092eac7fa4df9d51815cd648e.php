<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-10">
             <h1 class=""><?php echo e($user->name); ?></h1>
         
        <a href="<?php echo e(url('your-profile')); ?>" class="btn btn-info">Back to profile</a>
    <br>
        </div>
      <div class="col-sm-2"><a href="<?=url('fotoupload'.'/'.$user->is_photo)?>" class="pull-right"><img title="profile image" class="img-circle img-responsive" src="<?=url('fotoupload'.'/'.$user->is_photo)?>"></a>

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Language</div>

                <div class="panel-body">
                        <div class="row justify-content-center">
                            <h2>Languages</h2>
                            <br>
                            <br>
                            <div class='container'>
                            <?php echo Form::open([ 'action' => "StudentController@edit_profile" , 'method'=> 'POST', 'class' => 'form-horizontal']); ?>

                                <input type="hidden" name="student_id" value="<?php echo e(Auth::id()); ?>"> 
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-5">
                                        <div class="form-group">
                                          <label class="control-label">Select Your Language:</label>
                                            <select class="form-control" name="language" >
                                                <option value="">-select-</option>
                                                <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($language->lang_id == $user->is_language_id): ?>
                                                        <option value="<?php echo e($language->lang_id); ?>" selected><?php echo e($language->language); ?></option>
                                                    <?php else: ?>
                                                        <option value="<?php echo e($language->lang_id); ?>" ><?php echo e($language->language); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-4">
                                    <div class="form-group">        
                                        <input type="submit" class="btn btn-info  btn-md" name="submit_language" value='Change Language' />
                                    </div>
                                    </div>
                                </div>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Region</div>

                <div class="panel-body">
                        <div class="row justify-content-center">
                            <h2>Region</h2>
                            <br>
                            <br>
                            <div class='container'>
                            <?php echo Form::open([ 'action' => "StudentController@edit_profile" , 'method'=> 'POST', 'class' => 'form-horizontal']); ?>

                                
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-5">
                                        <div class="form-group">
                                            <label class="control-label">Time Zone:</label>
                                            <select class="form-control" name="time_zone" >
                                                <option value="">-select-</option>
                                                <option timeZoneId="1" gmtAdjustment="GMT-12:00" useDaylightTime="0" value="-12">(GMT-12:00) International Date Line West</option>
                                                <option timeZoneId="2" gmtAdjustment="GMT-11:00" useDaylightTime="0" value="-11">(GMT-11:00) Midway Island, Samoa</option>
                                                <option timeZoneId="3" gmtAdjustment="GMT-10:00" useDaylightTime="0" value="-10">(GMT-10:00) Hawaii</option>
                                                <option timeZoneId="4" gmtAdjustment="GMT-09:00" useDaylightTime="1" value="-9">(GMT-09:00) Alaska</option>
                                                <option timeZoneId="5" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="-8">(GMT-08:00) Pacific Time (US & Canada)</option>
                                                <option timeZoneId="6" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="-8">(GMT-08:00) Tijuana, Baja California</option>
                                                <option timeZoneId="7" gmtAdjustment="GMT-07:00" useDaylightTime="0" value="-7">(GMT-07:00) Arizona</option>
                                                <option timeZoneId="8" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="-7">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
                                                <option timeZoneId="9" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="-7">(GMT-07:00) Mountain Time (US & Canada)</option>
                                                <option timeZoneId="10" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="-6">(GMT-06:00) Central America</option>
                                                <option timeZoneId="11" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="-6">(GMT-06:00) Central Time (US & Canada)</option>
                                                <option timeZoneId="12" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="-6">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
                                                <option timeZoneId="13" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="-6">(GMT-06:00) Saskatchewan</option>
                                                <option timeZoneId="14" gmtAdjustment="GMT-05:00" useDaylightTime="0" value="-5">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
                                                <option timeZoneId="15" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="-5">(GMT-05:00) Eastern Time (US & Canada)</option>
                                                <option timeZoneId="16" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="-5">(GMT-05:00) Indiana (East)</option>
                                                <option timeZoneId="17" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="-4">(GMT-04:00) Atlantic Time (Canada)</option>
                                                <option timeZoneId="18" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="-4">(GMT-04:00) Caracas, La Paz</option>
                                                <option timeZoneId="19" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="-4">(GMT-04:00) Manaus</option>
                                                <option timeZoneId="20" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="-4">(GMT-04:00) Santiago</option>
                                                <option timeZoneId="21" gmtAdjustment="GMT-03:30" useDaylightTime="1" value="-3.5">(GMT-03:30) Newfoundland</option>
                                                <option timeZoneId="22" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Brasilia</option>
                                                <option timeZoneId="23" gmtAdjustment="GMT-03:00" useDaylightTime="0" value="-3">(GMT-03:00) Buenos Aires, Georgetown</option>
                                                <option timeZoneId="24" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Greenland</option>
                                                <option timeZoneId="25" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Montevideo</option>
                                                <option timeZoneId="26" gmtAdjustment="GMT-02:00" useDaylightTime="1" value="-2">(GMT-02:00) Mid-Atlantic</option>
                                                <option timeZoneId="27" gmtAdjustment="GMT-01:00" useDaylightTime="0" value="-1">(GMT-01:00) Cape Verde Is.</option>
                                                <option timeZoneId="28" gmtAdjustment="GMT-01:00" useDaylightTime="1" value="-1">(GMT-01:00) Azores</option>
                                                <option timeZoneId="29" gmtAdjustment="GMT+00:00" useDaylightTime="0" value="0">(GMT+00:00) Casablanca, Monrovia, Reykjavik</option>
                                                <option timeZoneId="30" gmtAdjustment="GMT+00:00" useDaylightTime="1" value="0">(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London</option>
                                                <option timeZoneId="31" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                                <option timeZoneId="32" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                                <option timeZoneId="33" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                                <option timeZoneId="34" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
                                                <option timeZoneId="35" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) West Central Africa</option>
                                                <option timeZoneId="36" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Amman</option>
                                                <option timeZoneId="37" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Athens, Bucharest, Istanbul</option>
                                                <option timeZoneId="38" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Beirut</option>
                                                <option timeZoneId="39" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Cairo</option>
                                                <option timeZoneId="40" gmtAdjustment="GMT+02:00" useDaylightTime="0" value="2">(GMT+02:00) Harare, Pretoria</option>
                                                <option timeZoneId="41" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
                                                <option timeZoneId="42" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Jerusalem</option>
                                                <option timeZoneId="43" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Minsk</option>
                                                <option timeZoneId="44" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Windhoek</option>
                                                <option timeZoneId="45" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Kuwait, Riyadh, Baghdad</option>
                                                <option timeZoneId="46" gmtAdjustment="GMT+03:00" useDaylightTime="1" value="3">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
                                                <option timeZoneId="47" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Nairobi</option>
                                                <option timeZoneId="48" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Tbilisi</option>
                                                <option timeZoneId="49" gmtAdjustment="GMT+03:30" useDaylightTime="1" value="3.5">(GMT+03:30) Tehran</option>
                                                <option timeZoneId="50" gmtAdjustment="GMT+04:00" useDaylightTime="0" value="4">(GMT+04:00) Abu Dhabi, Muscat</option>
                                                <option timeZoneId="51" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="4">(GMT+04:00) Baku</option>
                                                <option timeZoneId="52" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="4">(GMT+04:00) Yerevan</option>
                                                <option timeZoneId="53" gmtAdjustment="GMT+04:30" useDaylightTime="0" value="4.5">(GMT+04:30) Kabul</option>
                                                <option timeZoneId="54" gmtAdjustment="GMT+05:00" useDaylightTime="1" value="5">(GMT+05:00) Yekaterinburg</option>
                                                <option timeZoneId="55" gmtAdjustment="GMT+05:00" useDaylightTime="0" value="5">(GMT+05:00) Islamabad, Karachi, Tashkent</option>
                                                <option timeZoneId="56" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="5.5">(GMT+05:30) Sri Jayawardenapura</option>
                                                <option timeZoneId="57" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="5.5">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                                <option timeZoneId="58" gmtAdjustment="GMT+05:45" useDaylightTime="0" value="5.75">(GMT+05:45) Kathmandu</option>
                                                <option timeZoneId="59" gmtAdjustment="GMT+06:00" useDaylightTime="1" value="6">(GMT+06:00) Almaty, Novosibirsk</option>
                                                <option timeZoneId="60" gmtAdjustment="GMT+06:00" useDaylightTime="0" value="6">(GMT+06:00) Astana, Dhaka</option>
                                                <option timeZoneId="61" gmtAdjustment="GMT+06:30" useDaylightTime="0" value="6.5">(GMT+06:30) Yangon (Rangoon)</option>
                                                <option timeZoneId="62" gmtAdjustment="GMT+07:00" useDaylightTime="0" value="7">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
                                                <option timeZoneId="63" gmtAdjustment="GMT+07:00" useDaylightTime="1" value="7">(GMT+07:00) Krasnoyarsk</option>
                                                <option timeZoneId="64" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                                <option timeZoneId="65" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Kuala Lumpur, Singapore</option>
                                                <option timeZoneId="66" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
                                                <option timeZoneId="67" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Perth</option>
                                                <option timeZoneId="68" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Taipei</option>
                                                <option timeZoneId="69" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="9">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
                                                <option timeZoneId="70" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="9">(GMT+09:00) Seoul</option>
                                                <option timeZoneId="71" gmtAdjustment="GMT+09:00" useDaylightTime="1" value="9">(GMT+09:00) Yakutsk</option>
                                                <option timeZoneId="72" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="9.5">(GMT+09:30) Adelaide</option>
                                                <option timeZoneId="73" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="9.5">(GMT+09:30) Darwin</option>
                                                <option timeZoneId="74" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="10">(GMT+10:00) Brisbane</option>
                                                <option timeZoneId="75" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Canberra, Melbourne, Sydney</option>
                                                <option timeZoneId="76" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Hobart</option>
                                                <option timeZoneId="77" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="10">(GMT+10:00) Guam, Port Moresby</option>
                                                <option timeZoneId="78" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Vladivostok</option>
                                                <option timeZoneId="79" gmtAdjustment="GMT+11:00" useDaylightTime="1" value="11">(GMT+11:00) Magadan, Solomon Is., New Caledonia</option>
                                                <option timeZoneId="80" gmtAdjustment="GMT+12:00" useDaylightTime="1" value="12">(GMT+12:00) Auckland, Wellington</option>
                                                <option timeZoneId="81" gmtAdjustment="GMT+12:00" useDaylightTime="0" value="12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
                                                <option timeZoneId="82" gmtAdjustment="GMT+13:00" useDaylightTime="0" value="13">(GMT+13:00) Nuku'alofa</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                          <label class="control-label">Country:</label>
                                            <select class="form-control" name="country" >
                                                <option value="">-select-</option>
                                                <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($country->count_id == $user->is_country): ?>
                                                    <option value="<?php echo e($country->count_id); ?>" selected><?php echo e($country->country); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($country->count_id); ?>"><?php echo e($country->country); ?></option>
                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">City:</label>
                                            <select class="form-control" name="city" >
                                                <option value="">-select-</option>
                                                <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($city->city_id == $user->is_city): ?>
                                                    <option value="<?php echo e($city->city_id); ?>" selected><?php echo e($city->city); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($city->city_id); ?>"><?php echo e($city->city); ?></option>
                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-4">
                                    <div class="form-group">        
                                        <input type="submit" class="btn btn-info  btn-md" name="submit_region" value='Change Region' />
                                    </div>
                                    </div>
                                </div>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Personal Information</div>

                <div class="panel-body">
                        <div class="row justify-content-center">
                            <h2>Personal Information</h2>
                            <br>
                            <br>
                            <div class='container'>
                            <?php echo Form::open([ 'action' => "StudentController@edit_profile" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]); ?>

                                
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">DOB:</label>
                                            <input type='date' name='dob' class='form-control' value="<?php echo e($user->is_dob); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label">Gender:</label>
                                                <select class="form-control" name="gender" >
                                                        <?php $male='selected'; $female='' ?>
                                                        <?php if($user->is_gender == 'female'): ?>
                                                            <?php
                                                                $male='';
                                                                $female='selected';
                                                            ?>
                                                        <?php endif; ?>
                                                        <option value="">-select-</option>
                                                        <option value="male" <?php echo e($male); ?>>Male</option>
                                                        <option value="female" <?php echo e($female); ?>>Female</option>
                                                    </select>
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                          <label class="control-label">Phone:</label>
                                        <input type='text' name='phone' value='<?php echo e($user->is_phone); ?>' class='form-control' placeholder="Enter phone no.."/>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">Picure:</label>
                                            <?php echo e(Form::file('picture')); ?>

                                        </div>
                                    </div>
                                </div>

                                <div class='row'>
                                    <div class='col-sm-offset-1 col-sm-5'>
                                        <div class="form-group">
                                        <label class="control-label">Write short introduction abour youself:</label>
                                        <?php echo e(Form::textarea('info', $user->is_st_intro, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Type youn introduction'])); ?>

                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-4">
                                    <div class="form-group">        
                                        <input type="submit" class="btn btn-info  btn-md" name="submit_personal" value='Change info' />
                                    </div>
                                    </div>
                                </div>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Academic Information</div>

                <div class="panel-body">
                        <div class="row justify-content-center">
                            <h2>Academic Information</h2>
                            <br>
                            <br>
                            <div class='container'>
                            <?php echo Form::open([ 'action' => "StudentController@edit_profile" , 'method'=> 'POST', 'class' => 'form-horizontal', 'files' => true]); ?>

                                
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">Degree:</label>
                                            <select class="form-control" name="degree" >
                                                <option value="">-select-</option>
                                                <?php $__currentLoopData = $degrees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $degree): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    
                                                    <?php if($degree->deg_id == $user->is_qualification): ?>
                                                        <option value="<?php echo e($degree->deg_id); ?>" selected><?php echo e($degree->degree); ?></option>
                                                    <?php else: ?>
                                                        <option value="<?php echo e($degree->deg_id); ?>"><?php echo e($degree->degree); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">Institute:</label>
                                        <input type='text' class='form-control' name='institute' value='<?php echo e($user->is_institute); ?>' placeholder="Type institute name.." />
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">From year:</label>
                                            <select class="form-control" name="from" >
                                                <option value="">-select-</option>
                                                 <?php for($i = 2018; $i >= 1970; $i--): ?>
                                                    <?php if($i == $user->is_from): ?>
                                                        <option value="<?php echo e($i); ?>" selected><?php echo e($i); ?></option>
                                                    <?php else: ?>
                                                        <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                                    <?php endif; ?>
                                                 <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-1 col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">To year:</label>
                                            <select class="form-control" name="to" >
                                                <option value="">-select-</option>
                                                <?php if($user->is_to == 'progress'): ?>
                                                    <option value="progress" selected>In Progress</option>
                                                <?php endif; ?>
                                                <?php for($i = 2018; $i >= 1970; $i--): ?>
                                                    <?php if($i == $user->is_to): ?>
                                                        <option value="<?php echo e($i); ?>" selected><?php echo e($i); ?></option>
                                                    <?php else: ?>
                                                        <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                                    <?php endif; ?>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class='row'>
                                    <div class="col-sm-offset-1 col-sm-4">
                                    <div class="form-group">        
                                        <input type="submit" class="btn btn-info  btn-md" name="submit_academic" value='Change Record'/>
                                    </div>
                                    </div>
                                </div>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>