<?php $__env->startSection('content'); ?>

<hr class="">
<div class="container target">
    <div class="row">
        <div class="col-sm-3">
            <h1 class="">Find Teacher</h1>
        </div>
        <br>
        <br>
        <?php echo Form::open([ 'action' => "StudentController@find_teacher" , 'method'=> 'POST', 'class' => 'form-horizontal']); ?>

            <div class="col-sm-3">
            
                <select name = 'course_select' class="selectpicker form-control">
                    <option value='' disabled selected>select course</option>
                    <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($course->course_id); ?>"><?php echo e($course->course); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                          
            </div>
            <div class="col-sm-3">
                <input type="submit" name='search' class="btn btn-success" value="Search" />
            </div>
        </form>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-3">
            <!--left col-->
            <ul class="list-group">
                <li class="list-group-item text-muted" contenteditable="false">Profile</li>
                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Language</strong></span> ddfgdfg</li>
                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Gender</strong></span> sdfsdfsd</li>
            <li class="list-group-item text-right"><span class="pull-left"><strong class="">Lives in</strong></span> fsdfsdf</li>
              <li class="list-group-item text-right"><span class="pull-left"><strong class="">Qualification</strong></span> sfsdf</li>
            </ul>
           <div class="panel panel-default">
             <div class="panel-heading">Insured / Bonded?

                </div>
                <div class="panel-body"><i style="color:green" class="fa fa-check-square"></i> Yes, I am insured and bonded.

                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Website <i class="fa fa-link fa-1x"></i>

                </div>
                <div class="panel-body"><a href="http://bootply.com" class="">bootply.com</a>

                </div>
            </div>
          
            <ul class="list-group">
                <li class="list-group-item text-muted">Activity <i class="fa fa-dashboard fa-1x"></i>

                </li>
                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Shares</strong></span> 125</li>
                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Likes</strong></span> 13</li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong class="">Posts</strong></span> 37</li>
                        <li class="list-group-item text-right"><span class="pull-left"><strong class="">Followers</strong></span> 78</li>
            </ul>
            <div class="panel panel-default">
                <div class="panel-heading">Social Media</div>
                <div class="panel-body">	<i class="fa fa-facebook fa-2x"></i>  <i class="fa fa-github fa-2x"></i> 
                    <i class="fa fa-twitter fa-2x"></i> <i class="fa fa-pinterest fa-2x"></i>  <i class="fa fa-google-plus fa-2x"></i>

                </div>
            </div>
        </div>
        <!--/col-3-->
        <div class="col-sm-9" contenteditable="false" style="">

            <div class="panel panel-default">
                    
                <div class="panel-heading"><h3>Teachers</h3></div>
                <?php $__currentLoopData = $tc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
       
                <div class="panel-body">
                    <div class="row">
                        <div class='col-md-4'>
                            <img src="<?=url('fotoupload'.'/'.$value->it_photo)?>" class="img-thumbnail" alt="Cinque Terre" width="304" height="236">
                        </div>
                        <div class='col-md-8'>
                            <div class="row">
                                <b>Name: </b><?php echo e($value->name); ?>

                            </div>
                            <div class="row">
                                <b>Course: </b><?php echo e($value->course); ?>

                            </div>
                            <div class="row">
                                <b>Description: </b><?php echo $value->it_intro; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>

            <!-- <div class="panel panel-default target">

                <div class="panel-heading" contenteditable="false">My Lessons</div>

                <div class="panel-body">
              
                </div>
                 
            </div> -->
              
        </div>

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>