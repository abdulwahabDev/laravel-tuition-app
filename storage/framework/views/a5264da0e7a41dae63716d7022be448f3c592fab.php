<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile Setup  Step 01</div>

                <div class="panel-body">
                        <div class="row justify-content-center">
                            <h2>Language</h2>
                            <br>
                            <br>
                            <div class='container'>
                            <?php echo Form::open([ 'action' => "StudentController@profile_setup" , 'method'=> 'POST', 'class' => 'form-horizontal']); ?>

                                <input type="hidden" name="student_id" value="<?php echo e(Auth::id()); ?>"> 
                                <div class="row">
                                    <div class="col-sm-offset-2 col-sm-4">
                                        <div class="form-group">
                                          <label class="control-label">Select Your Language:</label>
                                            <select class="form-control" name="language" >
                                                <option value="">-select-</option>
                                                <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($language->lang_id); ?>"><?php echo e($language->language); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class="col-sm-offset-2">
                                    <div class="form-group">        
                                        <input type="submit" class="btn btn-info  btn-md" name="submit_language" />
                                    </div>
                                    </div>
                                </div>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>